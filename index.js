// console.log("Good Afternoon");

/*We can use prompt to collect data. However, for some use cases, this may not be ideal. For tother cases, functions can also process data directly passed into it instead of relying on Global Variable prompt()*/



// [Section] Parameters and Arguments
	// (name) - parameter. acts as named variable or container that exists only inside of a function.
	// ("Chris") - argument passing. the information or data provided directly into the function called an argument.
	
	function printName(name){
		console.log("My name is " + name);
	}

	printName("Chris");
	
	// You can directly pass data into the function. The function can then use that data which is referred as "name" within the function.

	printName("George");
	
	// Values passed when invoking a function are called arguments. These arguments are the stored as the parameters within the function.


	// Variables can also be passed as an argument.
		let sampleVariable = "Edward";
		printName(sampleVariable);


	// Function arguments cannot be used by a function if there are no parameters provided within the function.
		function noParams(){
			let params = "No parameter";
			console.log(params)
		}

		noParams("With Parameter");


		function checkDivisibilityBy8(num){
			let modulo = num%8;
			console.log("The remainder of " + num + " divided by 8 is: " + modulo);
		
			let isDivisibleBy8 = modulo === 0; /*true or false*/
			console.log("Is " + num + " divisible by 8?");

			console.log(isDivisibleBy8)
		}

		checkDivisibilityBy8(8);
		checkDivisibilityBy8(17);

	// You can also do the same using the prompt(), however, takte note that prompt() outputs a string. Strings are not ideal for mathematical computations.



// [Section] Functions as Arguments
	// Function parameters can also accept other functions as arguments. 
	// Some complex functions use other functions as arguments to perform more complicated results.
	// This will be futher seen when we discuss array methods.

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	}

	function argumentFunctionTwo(){
		console.log("This function was passed as an argument from the second argument function")
	}

	function invokeFunction(argFunction){
		argFunction(); /*Adding and removing the parentheses "()" impacts the output of Javascript heavily.*/
	}

	invokeFunction(argumentFunction);
	invokeFunction(argumentFunctionTwo);

	// Adding and removing the parentheses "()" impacts the output of Javascript heavily.
	// When a function is used with parentheses "()", it denotes invoking a function.
	// A function used without parentheses "()" is normally associated with using the function as an argument to another function.



// [Section] Using multiple parameters
// Multiple "arguments" will correspond to the number of "parameters" declared in a function in "succeedng order"

	function createFullName (firstName, middleName, lastName){ /*can add value inside the parameters*/
		console.log("This is firstName: " + firstName);
		console.log("This is middleName: " + middleName);
		console.log("This is lastName: " + lastName);
	}

	createFullName("Juan", "Dela", "Cruz");
	createFullName("Juan", "Dela", "Cruz", "Jr."); /*4th will not be previewed*/
	createFullName("Juan", "Dela Cruz"); /*3rd is undefined*/
	createFullName("Juan", undefined, "Cruz"); /* or "" */



// [Section] Using variables as arguments
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);



// [Section] Return Statement
	// The "return" statement allows us to output a value from a function to be passed to the line or block of codes that invoked the function.

/*Example 1*/
	function returnFullName(firstName, middleName, lastName){
		console.log(firstName + " " + middleName + " " + lastName);
	}
	returnFullName("Ada", "None", "Lovelace");

	function returnName(firstName, middleName, lastName){
		return firstName + " " + middleName + " " + lastName;
		console.log(firstName);
	}
	console.log(returnName("John", "Doe", "Smith"));

	let fullName = returnName("John", "Doe", "Smith");
	console.log("This is the console.log from fullName variable");
	console.log(fullName);

/*Example 2*/
	function printPlayerInfo(userName, level, job){
		console.log("Username: " + userName);
		console.log("Level: " + level);
		console.log("Job: " + job);

		return userName + " " + level + " " + job; /*output: Knight_White 95 Paladin*/
	}
	printPlayerInfo("Knight_White", 95, "Paladin");

	let user1 = printPlayerInfo("Knight_White", 95, "Paladin");
	console.log(user1); 

